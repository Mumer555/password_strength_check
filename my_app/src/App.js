import React, { useState } from "react";
import "./App.css";
import Checking from "./Checking";

const App = () => {
  const [curstatus , setStatus] = useState("");
  const [curPass, setPass] = useState("");
  
  const inputEvent = (event) => {
    setPass(event.target.value);
  };
  const checkStrength = () =>{
    setStatus(<Checking password = {curPass}/>);
  }

  return (
    <div class="container">
      <div class="brand-logo"></div>
      <div class="brand-title">Password Confidentiality Maintainer</div>
      <div class="inputs">
        <label>PASSWORD</label>
        <input
          id="password_text"
          type="password"
          placeholder="enter password here"
          onChange={inputEvent}
        />
        <button type="submit" onClick={checkStrength}>CHECK</button>
        <label className="strength"></label>
        <p>{curstatus}</p>
      </div>
    </div>
  );
};

export default App;
